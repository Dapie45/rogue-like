﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public static GameController Instance;
    public bool isPlayerTurn;
    public bool areEnemiesMoving;
    public int playerCurrentHealth = 50;
    public AudioClip gameOverSound;


    private BoardController boardController;
    private List<Enemy> enemies;
    private GameObject levelImage;
    private Text levelText;
    private bool settingUpGame;
    private int secondsUntilLevelStart = 2;
    private int currentLevel = 1;
    private GameObject menu;
    private Text menuText;

    void Awake () {
        if(Instance != null && Instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
        boardController = GetComponent<BoardController>();
        enemies = new List<Enemy>();
	}
	
    void Start()
    {
        InitializeGame();
        menu = GameObject.Find("menu");
        menuText = GameObject.Find("menuText").GetComponent<Text>();
    }

    private void InitializeGame()
    {
        levelImage = GameObject.Find("Level Image");
        levelText = GameObject.Find("Level Text").GetComponent<Text>();

        if (currentLevel > 1)
        {
            levelText.text = "Day " + currentLevel;
            levelImage.SetActive(true);
            menu = GameObject.Find("menu");
            menu.SetActive(false);
            
        }
       
        

       
        enemies.Clear();
        settingUpGame = false;
        boardController.SetupLevel(currentLevel);
        Invoke("DisableLevelImage", secondsUntilLevelStart);
        settingUpGame = true;
    }

    private void DisableLevelImage()
    {
        levelImage.SetActive(false);
        settingUpGame = false;
        isPlayerTurn = true;
        areEnemiesMoving = false;
    }
    public void Disablemenu()
    {
        menu.SetActive(false);
    }

    private void OnLevelWasLoaded(int levelLoaded)
    {
        currentLevel++;
        InitializeGame();
    }

	void Update () {
	if(isPlayerTurn || areEnemiesMoving || settingUpGame)
        {
            return;
        }
        StartCoroutine(MoveEnimies());
	}

    private IEnumerator MoveEnimies()
    {
        areEnemiesMoving = true;

        yield return new WaitForSeconds(0.2f);

        foreach(Enemy enemy in enemies)
        {
            enemy.MoveEnemy();
            yield return new WaitForSeconds(enemy.moveTime);
        }

        areEnemiesMoving = false;
        isPlayerTurn = true;
    }

    public void AddEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }

    private bool CheckHighscore()
    {
        if (currentLevel > PlayerPrefs.GetInt("highscore", 0))
        {
            return true;
        }
        else return false;
    }
    private int GetHighscore()
    {
        if( CheckHighscore() )
        {
            PlayerPrefs.SetInt("highscore", currentLevel);
        }
        return PlayerPrefs.GetInt("highscore", 0);
    }

    public void GameOver()
    {
        bool brokenHighscore = CheckHighscore();
        int highscore = GetHighscore();
        isPlayerTurn = false;
        SoundController.Instance.music.Stop();
        SoundController.Instance.PlaySingle(gameOverSound);
        if (brokenHighscore)
        {
            levelText.text = "You starved after " + currentLevel + " days...\n" + "Good Job, New Highscore: " + highscore;
        }
        else
        {
            levelText.text = "You starved after " + currentLevel + " days...\n" + "Old Highscore: " + highscore;
        }
        levelImage.SetActive(true);
        enabled = false;
    }
}
